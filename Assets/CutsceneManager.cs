using System;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class CutsceneManager : MonoBehaviour {
    public bool doingCutscene;
    public bool doingStarting;
    public int page;
    public string[] startingPages;
    public string[] endingPages;
    public PlayerInput playerInput;

    public Canvas cutsceneCanvas;
    public TMP_Text cutsceneText;

    public GameStateManager stateManager;

    public CameraFollow cameraFollow;
    private float _oldFollowSpeed;
    private Vector3 _oldCameraPosition;

    public void Start() {
        doingCutscene = true;
        doingStarting = true;
        _oldFollowSpeed = cameraFollow.followSpeed;
        _oldCameraPosition = cameraFollow.playerDelta;
        cameraFollow.followSpeed = 2f;
        cameraFollow.playerDelta = new Vector3(10, 25, 30);
    }

    public void OnProgress(InputAction.CallbackContext context) {
        if (context.performed) {
            page += 1;
            
            if (doingStarting && page == startingPages.Length) {
                doingCutscene = false;
                doingStarting = false;
                page = 0;
                playerInput.SwitchCurrentActionMap("Player");
                cameraFollow.followSpeed = _oldFollowSpeed;
                cameraFollow.playerDelta = _oldCameraPosition;
            } else if (!doingStarting && page == endingPages.Length) {
                playerInput.SwitchCurrentActionMap("MenuState");
                doingCutscene = false;
                stateManager.allowWinScreen = true;
            }
        }
    }

    public void BeginEndingCutscene() {
        doingCutscene = true;
        playerInput.SwitchCurrentActionMap("Cutscenes");
        cameraFollow.followSpeed = 2f;
        cameraFollow.playerDelta = new Vector3(50, 80, 50);
    }

    public void Update() {
        cutsceneCanvas.enabled = doingCutscene;
        if (doingCutscene) {
            if (doingStarting) {
                cutsceneText.text = startingPages[page];
            } else {
                cutsceneText.text = endingPages[page];
            }
        }
    }
}
