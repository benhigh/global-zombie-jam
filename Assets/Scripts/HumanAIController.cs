using UnityEngine;
using UnityEngine.AI;

public class HumanAIController : MonoBehaviour {
    public AudioClip[] scaredSounds;
    public float screamVolume;
    
    private StateMachine _stateMachine;
    private NavMeshAgent _agent;
    private HumanWanderState _humanWanderState;
    private HumanFleeState _humanFleeState;
    private IState _startingState;
    private GameObject _player;
    private HumanAttackState _humanAttackState;
    private HumanController _humanController;


    private FamilyFollowState _familyFollowState;
    private Animator _animator;
    private static readonly int Running = Animator.StringToHash("Running");
    
    private bool _willAttack;
    private bool _notWandering;
    public float maxTimerCheck = 10f;
    private float _timerCheck = 0f;
    private AudioSource _audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        _audioSource = this.GetComponent<AudioSource>();
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _stateMachine = new StateMachine();
        _humanController = GetComponent<HumanController>();
        _player = GameObject.FindGameObjectWithTag("Player");
        var self = gameObject;
        
        _humanWanderState = new HumanWanderState(_agent, self);
        _humanFleeState = new HumanFleeState(_agent, self, _player);
        _familyFollowState = new FamilyFollowState(_agent, self);
        _humanAttackState = new HumanAttackState(_agent, self, _player, _animator);
        
        _humanWanderState.wanderTimer = 2f;
        _startingState = _humanWanderState;
        Transition(_startingState);

        _willAttack = false;
        if (!GetComponent<HumanController>().isFamily)
        {
            if (Random.Range(0, 10) < 3)
            {
                _willAttack = true;
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        _stateMachine.Update();

        if (_agent.remainingDistance - _agent.stoppingDistance > 0.1) {
            _animator.SetBool(Running, true);
        } else {
            _animator.SetBool(Running, false);
        }

        if (_notWandering && !_humanController.infected)
        {
            _timerCheck += Time.deltaTime;
            if (_timerCheck > maxTimerCheck)
            {
                var distance =  Vector3.Distance(transform.position, _player.transform.position);
                if (distance > 16)
                {
                    Transition(_humanWanderState);
                }
                _timerCheck = 0;
            }
        }
    }
    
    private void Transition(IState iState)
    {
        _stateMachine.Transition(iState);
    }

    public void SetToFollow() {
        Transition(_familyFollowState);
    }

    public void StopFollowing() {
        Transition(_humanWanderState);
    }

    public void HeardScream()
    {
        
        System.Random random = new System.Random();
        int randNum = random.Next(scaredSounds.Length);
        _audioSource.volume = screamVolume;
        _audioSource.PlayOneShot(scaredSounds[randNum]);
        Debug.Log("Heard Scream, Will attack: " + _willAttack);
        if (!GetComponent<HumanController>().infected)
        {
            if (_willAttack)
            {
                Transition(_humanAttackState);
            }
            else
            {
                Transition(_humanFleeState);
            }
            _notWandering = true;
        }
    }
}
