using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {
    public Animator fadingAnimator;
    private static readonly int Fading = Animator.StringToHash("Fading");
    private AsyncOperation _sceneLoadOperation;

    private void Start() {
        Cursor.visible = true;
    }

    public void Update() {
        if (_sceneLoadOperation != null && _sceneLoadOperation.isDone) {
            _sceneLoadOperation.allowSceneActivation = true;
        }
    }

    public void OnPlayClicked() {
        if (_sceneLoadOperation == null) {
            _sceneLoadOperation = SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
        }
        Debug.Log("Play");
        fadingAnimator.SetBool(Fading, true);
    }

    public void OnExitClicked() {
        Application.Quit(0);
    }
}
