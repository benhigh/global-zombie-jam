using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

public class ScreamScript : MonoBehaviour
{
    
    public AudioClip zombieScream;
    public Canvas questCanvas;
    private AudioSource _audioSource;

    
    void Start() {
        _audioSource = this.GetComponent<AudioSource>();
        _audioSource.clip = zombieScream;
    }

    public IEnumerator Scream() {
        float time = 10f;
        _audioSource.Play();
        for (int i = 0; i < 10000; i++) {
            while (time >= 0) {
                questCanvas.enabled = true;
                time -= Time.smoothDeltaTime;
                yield return null;
            }
        }

        questCanvas.enabled = false;
    }



}
