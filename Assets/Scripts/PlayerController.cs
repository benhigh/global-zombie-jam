using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour {
    public float speed = 0.02f;
    public float turnSpeed = 0.01f;
    public float screamTime = 4f;
    public AudioClip zombieScream;
    public Canvas questCanvas;
    public float zombieRadius;
    public AudioClip[] footsteps;
    public float walkTimeBetweenSteps = 2;
    public float runTimeBetweenSteps = 1;
    public float footstepVolume;
    public float zombieScreamVolume;

    public bool running;
    public float runSpeedModifier;
    public float stamina;
    public float maxStamina;
    public float staminaDrain;
    public float staminaRecover;

    private Vector3 _velocity = Vector3.zero;
    private Quaternion _lookDirection = Quaternion.Euler(Vector3.forward);
    private Animator _animator;
    private static readonly int Running = Animator.StringToHash("Running");
    private AudioSource _audioSource;
    private LayerMask _humanLayerMask;
    private float _screamTimeLeft;
    private CharacterController _characterController;
    private float _stepTimeLeft = 0;
    private bool _canFootstep = true;
    private float _currentTimeBetweenSteps;
    
    public void Start() {
        _humanLayerMask = LayerMask.GetMask("Human");
        _audioSource = GetComponent<AudioSource>();
        _animator = GetComponent<Animator>();
        stamina = maxStamina;
        _characterController = GetComponent<CharacterController>();
    }

    public void OnMove(InputAction.CallbackContext context) {
        Vector2 direction = context.ReadValue<Vector2>();
        _velocity = new Vector3(direction.x, 0, direction.y);
    }

    public void OnLunge(InputAction.CallbackContext _) {
        Debug.Log("Lunge");
    }

    public void OnScream(InputAction.CallbackContext _) {
        if (_.performed) {
            _screamTimeLeft = screamTime;
            Debug.Log("screamed");
            Collider[] collidingHumans =
                Physics.OverlapSphere(this.transform.position, zombieRadius, _humanLayerMask);
            foreach (Collider humanCollider in collidingHumans) {
                GameObject human = humanCollider.gameObject;
                human.GetComponent<HumanAIController>().HeardScream();
            }
            _audioSource.volume = zombieScreamVolume;
            _audioSource.PlayOneShot(zombieScream);
            questCanvas.enabled = true;
        }
    }

    public void OnRun(InputAction.CallbackContext context) {
        if (context.performed) {
            running = true;
        } else if (context.canceled) {
            running = false;
        }
    }

    public void Update() {
        //check step timer
        _stepTimeLeft -= Time.deltaTime;
        if (_stepTimeLeft < 0) {
            _canFootstep = true;
        }
        //check scream timer
        _screamTimeLeft -= Time.deltaTime;
        if (_screamTimeLeft < 0) {
            questCanvas.enabled = false;
        }


        
        // Running logic
        float thisSpeed = speed;
        if (running) {
            _currentTimeBetweenSteps = runTimeBetweenSteps;
            if (stamina > 0) {
                if (!_velocity.Equals(Vector3.zero)) {
                    stamina -= staminaDrain * Time.deltaTime;
                }

                thisSpeed *= runSpeedModifier;
            } else {
                running = false;
            }
        } else {
            _currentTimeBetweenSteps = walkTimeBetweenSteps;
            if (stamina < maxStamina) {
                stamina = Mathf.Clamp(stamina + staminaRecover * Time.deltaTime, 0, maxStamina);
            }
        }
        
        //if moving, play footstep sound
        if (_animator.GetBool("Running") == true || _animator.GetBool("Walking") == true) {
            if (_canFootstep) {
                _stepTimeLeft = _currentTimeBetweenSteps;
                _canFootstep = false;
                System.Random random = new System.Random();
                int randNum = random.Next(footsteps.Length);
                _audioSource.volume = footstepVolume;
                _audioSource.PlayOneShot(footsteps[randNum]);
            }
        }
        
        // Movement logic
        if (!_velocity.Equals(Vector3.zero)) {
            _animator.SetBool(Running, true);
            //transform.Translate(Vector3.forward * thisSpeed * Time.deltaTime);
            _characterController.Move(transform.forward * thisSpeed * Time.deltaTime);
            _lookDirection = Quaternion.LookRotation(_velocity, Vector3.up);
        } else {
            _animator.SetBool(Running, false);
        }
        
        // Rotation logic
        transform.rotation = Quaternion.Lerp(transform.rotation, _lookDirection, Time.deltaTime * turnSpeed);
    }
}
