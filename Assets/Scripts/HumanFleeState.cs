﻿using UnityEngine;
using UnityEngine.AI;

public class HumanFleeState: IState
{
    private readonly NavMeshAgent _agent;
    private readonly GameObject _self;
    private readonly GameObject _player;
    private float distance = 10f;
    public float timeToWait = 10f;
    private float timer;

    public HumanFleeState(NavMeshAgent agent, GameObject self, GameObject player)
    {
        _agent = agent;
        _self = self;
        _player = player;
    }

    public void Enter()
    {
        _agent.speed = 7f;
        timer = timeToWait;
    }

    public void Execute()
    {
        Vector3 moveDir = _self.transform.position - _player.transform.position;
        timer += Time.deltaTime;
        if (timer >= timeToWait)
        {
            _agent.SetDestination(moveDir * distance);
            timer = 0;
        }
    }

    public void Exit()
    {
        
    }
    
}