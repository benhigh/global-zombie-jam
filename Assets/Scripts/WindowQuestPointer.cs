using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

using UnityEngine.UIElements;

public class WindowQuestPointer : MonoBehaviour {
    public Vector3 GraveyardPosition;
    public GameObject player;
    public static bool targetIsGrave = false; 
    private Vector3 _targetPosition;
    private RectTransform _pointerRectTransform;
    private float _borderSize = 100;
    public FamilyManager familyManager;
    [SerializeField] Camera _uiCamera;
    private GameStateManager _stateManager;
    
    private void Awake() {
       _pointerRectTransform = transform.Find("Pointer").GetComponent<RectTransform>();
       _stateManager = GameObject.FindWithTag("GameController").GetComponent<GameStateManager>();
    }

    private void Update() {
        _targetPosition = FindClosestFamily();
        Vector3 toPosition = _targetPosition;
        Vector3 fromPosition = Camera.main.transform.position;
        fromPosition.z = 0f;
        Vector3 direction = (toPosition - fromPosition).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        if (angle < 0) {
            angle += 360;
        }
        _pointerRectTransform.localEulerAngles = new Vector3(0,0,angle);

        Vector3 targetPositionScreenPoint = Camera.main.WorldToScreenPoint(_targetPosition);
        bool isOffScreen = targetPositionScreenPoint.x <= _borderSize || targetPositionScreenPoint.x >= Screen.width - _borderSize||
                           targetPositionScreenPoint.y <= _borderSize || targetPositionScreenPoint.y >= Screen.height - _borderSize;
        if (isOffScreen) {
            Vector3 cappedTargetScreenPosition = targetPositionScreenPoint;
            if (cappedTargetScreenPosition.x <= _borderSize) cappedTargetScreenPosition.x = _borderSize;
            if (cappedTargetScreenPosition.x >= Screen.width - _borderSize) cappedTargetScreenPosition.x = Screen.width - _borderSize;
            if (cappedTargetScreenPosition.y <= _borderSize) cappedTargetScreenPosition.y = _borderSize;
            if (cappedTargetScreenPosition.y >= Screen.height - _borderSize) cappedTargetScreenPosition.y = Screen.height - _borderSize;

            Vector3 pointerWorldPosition = _uiCamera.ScreenToWorldPoint(cappedTargetScreenPosition);
            _pointerRectTransform.position = pointerWorldPosition;
            _pointerRectTransform.localPosition = new Vector3(_pointerRectTransform.localPosition.x,_pointerRectTransform.localPosition.y, 0f);
        }
        else {
            Vector3 pointerWorldPosition = _uiCamera.ScreenToWorldPoint(targetPositionScreenPoint);
            _pointerRectTransform.position = pointerWorldPosition;
            _pointerRectTransform.localPosition = new Vector3(_pointerRectTransform.localPosition.x,_pointerRectTransform.localPosition.y, 0f);
        }
    }
    
    private Vector3 FindClosestFamily() {
        if (_stateManager.playerBeingFollowed) {
            return GraveyardPosition;
        }
        
        // get non infected family
        List<HumanController> validFamily = familyManager.familyMembers.FindAll(o => !o.infected);

        // find closest family member
        HumanController closestFamilyMember = null;
        Vector3 playerLoc = player.transform.position;
        foreach (HumanController member in validFamily) {
            if (closestFamilyMember == null || Vector3.Distance(member.transform.position, playerLoc) <
                Vector3.Distance(closestFamilyMember.transform.position, playerLoc)) {
                closestFamilyMember = member;
            }
        }

        if (closestFamilyMember != null) {
            return closestFamilyMember.transform.position;
        }

        return GraveyardPosition;

        //if length is an even number then go to the grave site instead
        // if (familyMembers.Length > 0) {
        //     return familyMembers[0].transform.position;
        // }
        // else {
        //     return GraveyardPosition;
        // }GraveyardPosition
    }

    

    }
