using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public GameObject player;
    public Vector3 playerDelta;
    public float followSpeed;

    private void Update() {
        transform.position = Vector3.Lerp(transform.position,
            player.transform.position + playerDelta,
            Time.deltaTime * followSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(player.transform.position - transform.position), Time.deltaTime * followSpeed);
    }
}
