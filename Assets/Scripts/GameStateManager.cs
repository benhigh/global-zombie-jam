using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour {
    public PlayerCombatController combatController;
    public FamilyManager familyManager;
    public Animator loseCanvasAnimator;
    public Animator winCanvasAnimator;
    public PlayerInput playerInput;
    private static readonly int PlayAnimation = Animator.StringToHash("PlayAnimation");
    public bool showingLoseScreen;
    public bool showingWinScreen;
    public AudioClip loseSound;
    public AudioClip winSound;
    public AudioSource audioSource;
    public bool allowWinScreen;
    public CutsceneManager cutsceneManager;
    public bool playerBeingFollowed;

    private void Start() {
        Cursor.visible = false;
    }

    private void Update() {
        if (allowWinScreen && familyManager.familyMembersCollected >= familyManager.numberOfFamilyMembers && !showingWinScreen) {
            audioSource.PlayOneShot(winSound);
            showingWinScreen = true;
            winCanvasAnimator.SetBool(PlayAnimation, true);
            playerInput.SwitchCurrentActionMap("MenuState");
        }
        
        if (familyManager.familyMembersCollected < familyManager.numberOfFamilyMembers && combatController.health <= 0 && !showingLoseScreen && !showingWinScreen) {
            audioSource.PlayOneShot(loseSound);
            showingLoseScreen = true;
            loseCanvasAnimator.SetBool(PlayAnimation, true);
            playerInput.SwitchCurrentActionMap("MenuState");
        }

        if (!allowWinScreen && familyManager.familyMembersCollected >= familyManager.numberOfFamilyMembers) {
            cutsceneManager.BeginEndingCutscene();
        }
    }

    public void RestartGame(InputAction.CallbackContext context) {
        if (context.performed) {
            SceneManager.LoadScene(1);
        }
    }
}
