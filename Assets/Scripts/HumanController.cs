using System;
using UnityEngine;

public class HumanController : MonoBehaviour {
    public enum HumanState {
        Wandering,
        Aggressive,
        Following,
        Idle
    }

    public HumanState state;
    public bool isFamily;
    public Material zombieSkin;
    public bool infected;

    public SkinnedMeshRenderer meshRenderer;

    private HumanAIController _aiController;

    private GameStateManager _stateManager;

    private void Start() {
        _aiController = GetComponent<HumanAIController>();
        state = HumanState.Wandering;
        _stateManager = GameObject.FindWithTag("GameController").GetComponent<GameStateManager>();
    }

    public void Infect()
    {
        meshRenderer.material = zombieSkin;
        infected = true;
        tag = "Human";
        WindowQuestPointer.targetIsGrave = true;
        if (isFamily && state == HumanState.Wandering)
        {
            _aiController.SetToFollow();
            state = HumanState.Following;
        }
        else
        {
            _aiController.StopFollowing();
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("GraveyardTrigger") && isFamily && state == HumanState.Following) {
            _aiController.StopFollowing();
            state = HumanState.Idle;
            GameObject.FindWithTag("GameController").GetComponent<FamilyManager>().familyMembersCollected += 1;
            _stateManager.playerBeingFollowed = false;
        }
    }
}