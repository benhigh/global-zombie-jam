﻿using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.AI;

public class HumanAttackState: IState
{
    private NavMeshAgent _agent;
    private GameObject _self;
    private GameObject _player;
    private Animator _anim;
    private float timeToWaitMove = 0.5f;
    private float timeToAttackMove = 1.5f;
    private float attackTimer = 0f;
    private float moveTimer;
    private static readonly int Idle = Animator.StringToHash("Idle");
    private float attackRadius = 3f;
    private PlayerCombatController _playerCombatController;
    private static readonly int attack = Animator.StringToHash("Attack");

    public HumanAttackState(NavMeshAgent agent, GameObject self, GameObject player, Animator anim)
    {
        _agent = agent;
        _self = self;
        _player = player;
        _anim = anim;
    }

    public void Enter()
    {
        _agent.speed = 6f;
        moveTimer = timeToWaitMove;
        _agent.stoppingDistance = 2f;
        _playerCombatController = _player.GetComponent<PlayerCombatController>();
    }

    public void Execute()
    {
        moveTimer += Time.deltaTime;
        var distance = Vector3.Distance(_self.transform.position, _player.transform.position);
        if (moveTimer >= timeToWaitMove)
        {
            if (distance > 1)
            {
                _agent.SetDestination(_player.transform.position);
            }
            else
            {
                _agent.ResetPath();
            }
            moveTimer = 0;
        }
        
        attackTimer += Time.deltaTime;
        if (attackTimer >= timeToAttackMove)
        {
            attackTimer = 0;
            if (distance - _agent.stoppingDistance < 0.6)
            {
                Attack();  
            }
        }
    }

    public void Exit()
    {
        _agent.stoppingDistance = 0f;
    }

    private void Attack()
    {
        Collider[] collidingObjects =
            Physics.OverlapSphere(_self.transform.position + _self.transform.forward + new Vector3(0, 1, 0), attackRadius);
        foreach (var col in collidingObjects)
        {
            if (col.gameObject.CompareTag("Player"))
            {
                _playerCombatController.Hit();
            }
        }
        _anim.SetTrigger(attack);
    }
}