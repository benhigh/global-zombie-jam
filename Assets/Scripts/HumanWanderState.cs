﻿using UnityEngine;
using UnityEngine.AI;

public class HumanWanderState: IState
{
    private NavMeshAgent _agent;
    private GameObject _self;
    public float range = 10.0f;
    public float wanderTimer;
    private float timer;
    
    public HumanWanderState(NavMeshAgent agent, GameObject self)
    {
        _agent = agent;
        _self = self;
    }
    
    public void Enter()
    {
        _agent.speed = 3f;
        timer = wanderTimer;
    }

    public void Execute()
    {
        timer += Time.deltaTime;
        Vector3 point;
        if (RandomPoint(_self.transform.position, range, out point))
        {
            if (timer >= wanderTimer)
            {
                _agent.SetDestination(point);
                timer = 0;
            }
        }
    }

    public void Exit()
    {
    }

    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
        
            NavMeshHit navHit;

            if (NavMesh.SamplePosition(randomPoint, out navHit, 1.0f, NavMesh.AllAreas))
            {
                result = navHit.position;
                return true;
            }
        }

        result = Vector3.zero;
        return false;
    }
}