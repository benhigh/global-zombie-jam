using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FamilyManager : MonoBehaviour {
    public int numberOfFamilyMembers;
    public int familyMembersCollected;
    public List<HumanController> familyMembers;
}
