using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerCombatController : MonoBehaviour {
    public GameObject attackSource;
    public float attackRadius;
    public float attackCooldown;
    public bool attackCharging;
    public float chargeTime;
    public AudioClip painSound;
    public AudioClip[] wooshSounds;
    public float wooshVolume;
    public float painVolume;

    public float health;
    public float maxHealth;
    
    private LayerMask _humanLayerMask;

    private Animator _animator;
    private static readonly int Lunge = Animator.StringToHash("Lunge");
    private AudioSource _audioSource;

    public GameStateManager stateManager;

    public void Start() {
        _audioSource = this.GetComponent<AudioSource>();
        _humanLayerMask = LayerMask.GetMask("Human");
        health = maxHealth;
        _animator = GetComponent<Animator>();
    }

    public void OnLunge(InputAction.CallbackContext context) {
        if (context.started) {
            // Attack charge started
            attackCharging = !stateManager.playerBeingFollowed;
            chargeTime = 0.0f;
        } else if (context.canceled) {
            // Attack charge canceled
            attackCharging = false;
        }
    }

    public void PerformAttack() {
        Collider[] collidingHumans =
            Physics.OverlapSphere(attackSource.transform.position, attackRadius, _humanLayerMask);
        
        _animator.SetTrigger(Lunge);
        
        GameObject closestHuman = null;
        float shortestDistance = attackRadius * 10;
        foreach (Collider humanCollider in collidingHumans) {
            float distance = Vector3.Distance(humanCollider.transform.position, transform.position);
            GameObject human = humanCollider.gameObject;
            if (distance < shortestDistance && !human.GetComponent<HumanController>().infected) {
                shortestDistance = distance;
                closestHuman = human;
            }
        }

        if (closestHuman != null) {
            closestHuman.GetComponent<HumanController>().Infect();

            if (closestHuman.GetComponent<HumanController>().isFamily) {
                stateManager.playerBeingFollowed = true;
            }
        }
    }
    
    public void Update() {
        if (attackCharging && chargeTime < attackCooldown) {
            // During attack charge
            chargeTime += Time.deltaTime;
        } else if (attackCharging && chargeTime >= attackCooldown) {
            // Attack charge complete
            System.Random random = new System.Random();
            int randNum = random.Next(wooshSounds.Length);
            _audioSource.volume = wooshVolume;
            _audioSource.PlayOneShot(wooshSounds[randNum]);
            PerformAttack();
            attackCharging = false;
        }
    }

    public void OnDrawGizmosSelected() {
        if (attackSource) {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(attackSource.transform.position, attackRadius);
        }
    }

    public void Hit() {
        _audioSource.volume = painVolume;
        _audioSource.PlayOneShot(painSound);
        health -= 10;
    }
}
