﻿using UnityEngine;
using UnityEngine.AI;

public class FamilyFollowState : IState {
    private NavMeshAgent _agent;
    private GameObject _self;
    public float distanceFromPlayer = 3;
    public float refreshTime = 0.5f;
    private float _timer;
    private GameObject _player;

    public FamilyFollowState(NavMeshAgent agent, GameObject self) {
        _agent = agent;
        _self = self;
        _player = GameObject.FindWithTag("Player");
    }
    
    public void Enter() {
        _agent.speed = 5f;
        SetPlayerDestination();
    }

    public void Execute() {
        _timer += Time.deltaTime;
        if (_timer >= refreshTime) {
            SetPlayerDestination();
            _timer = 0;
        }
    }

    private void SetPlayerDestination() {
        _agent.SetDestination(_player.transform.position - (_player.transform.forward * distanceFromPlayer));
    }

    public void Exit() {
    }
}