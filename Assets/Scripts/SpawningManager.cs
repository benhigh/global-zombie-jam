using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class SpawningManager : MonoBehaviour {
    public GameObject humanPrefab;
    public int numberOfHumans;
    public Material[] humanMaterials;
    public Material[] zombieMaterials;
    public GameObject[] spawnZones;
    public float spawnZoneRadius;
    private FamilyManager _familyManager;
    public static int numFamilyMembers;

    private void Start() {
        _familyManager = GetComponent<FamilyManager>();
        System.Random rng = new System.Random();
        
        for (int i = 0; i < numberOfHumans; i++) {
            // Find where we're spawning this human
            GameObject zone = spawnZones[Random.Range(0, spawnZones.Length)];
            Vector3 zonePosition = zone.transform.position;
            Vector3 spawnLocation = new Vector3(
                Random.Range(zonePosition.x - spawnZoneRadius, zonePosition.x + spawnZoneRadius),
                0, 
                Random.Range(zonePosition.z - spawnZoneRadius, zonePosition.z + spawnZoneRadius));

            // Pick a material
            int materialIndex = Random.Range(0, humanMaterials.Length);
            
            // Instantiate and place the human
            GameObject human = Instantiate(humanPrefab);
            //human.transform.position = spawnLocation;
            human.GetComponent<NavMeshAgent>().Warp(spawnLocation);

            // Set its materials
            HumanController humanController = human.GetComponent<HumanController>();
            humanController.zombieSkin = zombieMaterials[materialIndex];
            humanController.meshRenderer.material = humanMaterials[materialIndex];

            // Decide if family
            if (i == numberOfHumans - 1 && _familyManager.numberOfFamilyMembers == 0) {
                humanController.isFamily = true;
            } else {
                humanController.isFamily = rng.Next(100) <= 12;
            }

            if (humanController.isFamily) {
                _familyManager.familyMembers.Add(humanController);
                _familyManager.numberOfFamilyMembers += 1;
            }

            if (humanController.isFamily) {
                humanController.tag = "Family";
            }
        }

        numFamilyMembers = _familyManager.numberOfFamilyMembers;
    }

    private void OnDrawGizmosSelected() {
        foreach (GameObject spawnZone in spawnZones) {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(spawnZone.transform.position, spawnZoneRadius);
        }
    }
}
