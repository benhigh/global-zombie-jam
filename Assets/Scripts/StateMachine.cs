﻿// Adapted from https://forum.unity.com/threads/c-proper-state-machine.380612/

using UnityEngine;
using UnityEngine.AI;

public interface IState
{
    void Enter();
    void Execute();
    void Exit();
}
    
public class StateMachine
{
    public IState currentState;

    public void Transition(IState newState)
    {
        currentState?.Exit();

        currentState = newState;
        currentState.Enter();
    }

    public void Update()
    {
        currentState?.Execute();
    }
}