using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour {
    public PlayerCombatController playerCombatController;
    public PlayerController playerController;
    public Transform cameraObject;

    public FamilyManager familyManager;
    public TMP_Text collectedFamilyText;
    public TMP_Text totalFamilyText;
    
    public float fadeSpeed;
    
    public RectTransform healthBar;
    public Image healthBg;
    public Image healthFg;

    public RectTransform staminaBar;
    public Image staminaBg;
    public Image staminaFg;

    public Image healthIcon;
    public Image staminaIcon;

    public Transform chargeCanvas;
    public RectTransform chargeBar;
    public Image chargeBg;
    public Image chargeFg;
    
    private Color GetFadeColor(Color oldColor, float destinationAlpha) {
        return new Color(oldColor.r, oldColor.g, oldColor.b,
            Mathf.Lerp(oldColor.a, destinationAlpha, Time.deltaTime * fadeSpeed));
    }
    
    private void Update() {
        chargeCanvas.LookAt(cameraObject.transform.position);
        healthBar.sizeDelta = Vector2.Lerp(
            healthBar.sizeDelta,
            new Vector2(300 * (playerCombatController.health / playerCombatController.maxHealth), 20),
            Time.deltaTime);
        staminaBar.sizeDelta = new Vector2(300 * (playerController.stamina / playerController.maxStamina), 20);
        chargeBar.sizeDelta = new Vector2(200 * (playerCombatController.chargeTime / playerCombatController.attackCooldown), 40);

        if (playerCombatController.health < playerCombatController.maxHealth) {
            healthBg.color = GetFadeColor(healthBg.color, 0.5f);
            healthFg.color = GetFadeColor(healthFg.color, 1);
            healthIcon.color = GetFadeColor(healthIcon.color, 1);
        } else {
            healthBg.color = GetFadeColor(healthBg.color, 0);
            healthFg.color = GetFadeColor(healthFg.color, 0);
            healthIcon.color = GetFadeColor(healthIcon.color, 0);
        }
        
        if (playerController.running) {
            staminaBg.color = GetFadeColor(staminaBg.color, 0.5f);
            staminaFg.color = GetFadeColor(staminaFg.color, 1);
            staminaIcon.color = GetFadeColor(staminaIcon.color, 1);
        } else {
            staminaBg.color = GetFadeColor(staminaBg.color, 0);
            staminaFg.color = GetFadeColor(staminaFg.color, 0);
            staminaIcon.color = GetFadeColor(staminaIcon.color, 0);
        }

        if (playerCombatController.attackCharging) {
            chargeBg.color = GetFadeColor(chargeBg.color, 0.5f);
            chargeFg.color = GetFadeColor(chargeFg.color, 1);
        } else {
            chargeBg.color = GetFadeColor(chargeBg.color, 0);
            chargeFg.color = GetFadeColor(chargeFg.color, 0);
        }

        collectedFamilyText.text = familyManager.familyMembersCollected.ToString();
        totalFamilyText.text = familyManager.numberOfFamilyMembers.ToString();
    }
}
