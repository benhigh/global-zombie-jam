// GENERATED AUTOMATICALLY FROM 'Assets/Input/InputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""5f3d5741-6f30-4820-851d-d738d067a352"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""d62529b9-5c09-4b74-a67a-8a997707b96a"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Lunge"",
                    ""type"": ""Button"",
                    ""id"": ""855082b3-81f1-4d2c-b0d4-5336c3ea53fd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Scream"",
                    ""type"": ""Button"",
                    ""id"": ""728574fe-8b39-44aa-b631-f1d50102eeca"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Run"",
                    ""type"": ""Button"",
                    ""id"": ""f2949ff1-0474-4542-bf6e-0ad26f9a563c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""4d3a4139-9665-4ad4-b0d1-49c92f7338d8"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""9e40126a-aac4-4e60-96d5-c72e29b84d7d"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""7a787bc9-91f1-455a-a43c-6561d69e3081"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""58cf8a5a-91bf-4344-9080-63159697a505"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""f18c7892-6602-4532-8fe6-04f8866d3c32"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""066e6589-147a-4364-b1c3-f5383860c9ca"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Lunge"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""48f16e75-c85e-4a2b-8909-019a466ac50a"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Scream"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3ad81ad7-88e5-4820-9843-bac155d34b72"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""MenuState"",
            ""id"": ""da4f0537-a427-4c40-936f-ec6d9883d269"",
            ""actions"": [
                {
                    ""name"": ""Restart"",
                    ""type"": ""Button"",
                    ""id"": ""89ddd8fa-279f-4bf8-88ac-ec84f689f88d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""00282116-bccc-459a-ae71-7559bd19b38a"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Cutscenes"",
            ""id"": ""f905adc4-bc38-4c7d-90e9-4014ffb036a9"",
            ""actions"": [
                {
                    ""name"": ""Next"",
                    ""type"": ""Button"",
                    ""id"": ""25a11b96-27d2-49cc-84aa-b9aa9e0be30d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""40082dd8-bea6-4ed1-809d-d96b90bbe258"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Next"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Move = m_Player.FindAction("Move", throwIfNotFound: true);
        m_Player_Lunge = m_Player.FindAction("Lunge", throwIfNotFound: true);
        m_Player_Scream = m_Player.FindAction("Scream", throwIfNotFound: true);
        m_Player_Run = m_Player.FindAction("Run", throwIfNotFound: true);
        // MenuState
        m_MenuState = asset.FindActionMap("MenuState", throwIfNotFound: true);
        m_MenuState_Restart = m_MenuState.FindAction("Restart", throwIfNotFound: true);
        // Cutscenes
        m_Cutscenes = asset.FindActionMap("Cutscenes", throwIfNotFound: true);
        m_Cutscenes_Next = m_Cutscenes.FindAction("Next", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Move;
    private readonly InputAction m_Player_Lunge;
    private readonly InputAction m_Player_Scream;
    private readonly InputAction m_Player_Run;
    public struct PlayerActions
    {
        private @InputActions m_Wrapper;
        public PlayerActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Player_Move;
        public InputAction @Lunge => m_Wrapper.m_Player_Lunge;
        public InputAction @Scream => m_Wrapper.m_Player_Scream;
        public InputAction @Run => m_Wrapper.m_Player_Run;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Lunge.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLunge;
                @Lunge.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLunge;
                @Lunge.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLunge;
                @Scream.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnScream;
                @Scream.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnScream;
                @Scream.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnScream;
                @Run.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRun;
                @Run.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRun;
                @Run.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRun;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Lunge.started += instance.OnLunge;
                @Lunge.performed += instance.OnLunge;
                @Lunge.canceled += instance.OnLunge;
                @Scream.started += instance.OnScream;
                @Scream.performed += instance.OnScream;
                @Scream.canceled += instance.OnScream;
                @Run.started += instance.OnRun;
                @Run.performed += instance.OnRun;
                @Run.canceled += instance.OnRun;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);

    // MenuState
    private readonly InputActionMap m_MenuState;
    private IMenuStateActions m_MenuStateActionsCallbackInterface;
    private readonly InputAction m_MenuState_Restart;
    public struct MenuStateActions
    {
        private @InputActions m_Wrapper;
        public MenuStateActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Restart => m_Wrapper.m_MenuState_Restart;
        public InputActionMap Get() { return m_Wrapper.m_MenuState; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuStateActions set) { return set.Get(); }
        public void SetCallbacks(IMenuStateActions instance)
        {
            if (m_Wrapper.m_MenuStateActionsCallbackInterface != null)
            {
                @Restart.started -= m_Wrapper.m_MenuStateActionsCallbackInterface.OnRestart;
                @Restart.performed -= m_Wrapper.m_MenuStateActionsCallbackInterface.OnRestart;
                @Restart.canceled -= m_Wrapper.m_MenuStateActionsCallbackInterface.OnRestart;
            }
            m_Wrapper.m_MenuStateActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Restart.started += instance.OnRestart;
                @Restart.performed += instance.OnRestart;
                @Restart.canceled += instance.OnRestart;
            }
        }
    }
    public MenuStateActions @MenuState => new MenuStateActions(this);

    // Cutscenes
    private readonly InputActionMap m_Cutscenes;
    private ICutscenesActions m_CutscenesActionsCallbackInterface;
    private readonly InputAction m_Cutscenes_Next;
    public struct CutscenesActions
    {
        private @InputActions m_Wrapper;
        public CutscenesActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Next => m_Wrapper.m_Cutscenes_Next;
        public InputActionMap Get() { return m_Wrapper.m_Cutscenes; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CutscenesActions set) { return set.Get(); }
        public void SetCallbacks(ICutscenesActions instance)
        {
            if (m_Wrapper.m_CutscenesActionsCallbackInterface != null)
            {
                @Next.started -= m_Wrapper.m_CutscenesActionsCallbackInterface.OnNext;
                @Next.performed -= m_Wrapper.m_CutscenesActionsCallbackInterface.OnNext;
                @Next.canceled -= m_Wrapper.m_CutscenesActionsCallbackInterface.OnNext;
            }
            m_Wrapper.m_CutscenesActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Next.started += instance.OnNext;
                @Next.performed += instance.OnNext;
                @Next.canceled += instance.OnNext;
            }
        }
    }
    public CutscenesActions @Cutscenes => new CutscenesActions(this);
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLunge(InputAction.CallbackContext context);
        void OnScream(InputAction.CallbackContext context);
        void OnRun(InputAction.CallbackContext context);
    }
    public interface IMenuStateActions
    {
        void OnRestart(InputAction.CallbackContext context);
    }
    public interface ICutscenesActions
    {
        void OnNext(InputAction.CallbackContext context);
    }
}
